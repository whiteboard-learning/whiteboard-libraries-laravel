<?php

namespace Whiteboard\Middleware\Auth;

use Whiteboard\Services\HTTP\APIResponseService;
use Whiteboard\Services\Authentication\AuthenticationService;

use Closure;

class AllowIfAuthenticated {
    /**
     * @var App\Services\Auth\AuthenticationService Authentication Service
     */
    protected $service;

    public function __construct(AuthenticationService $service) {
        $this->service = $service;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  AccessTokenRepository $accessTokenRep
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {
        // check session
        $session = $this->service->verifyAccessToken($request->input('accessToken'));

        if ($session->getStatus() == "VALID") {
            return $next($request);
        }

        else {
            $response = new ApiResponseService($request);
            $response->setStatus('FORBIDDEN-AUTH');
            $response->appendError('Access forbidden.', 'You need to be logged in to view this page.');
            return response()->json($response->output());
        }
    }
}
