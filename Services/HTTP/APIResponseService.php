<?php

namespace Whiteboard\Services\HTTP;

use App\Services\Service;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Http\Request;
use Carbon\Carbon;

class APIResponseService {
    /**
     * Response Headers
     * Includes Start Time, End Time, Response Time, Requested Endpoint
     */
    private $headers = [];

    /**
     * Authentication Information
     * Includes current user access token, validation expiration, etc
     */
    private $authentication;

    /**
     * Status Code
     */
    public $status = "UNKNOWN";

    /**
     * Response Data
     */
    public $data = [];

    /**
     * Success Messages
     */
    public $successMessages = [];

    /**
     * Warnings
     */
    public $warnings = [];

    /**
     * Errors
     */
    public $errors = [];

    /**
     * Redirections
     */
    public $redirect = [];

    /**
     * Initialise the ResponseService object,
     * which is mainly for the headers
     */
    public function __construct() {
        $this->headers = [
            'service' => env('APP_SERVICE_NAME'),
            'serverID' => gethostname(),
            'responseTime' => Carbon::now()->toDateTimeString()
        ];
    }

    /**
     * Set status
     * 
     * @param array $status Status Code
     */
    public function setStatus($status) {
        $this->status = $status;
    }

    /**
     * Set data
     * 
     * @param array $data Data
     */
    public function setData($data) {
        $this->data = $data;
    }

    /**
     * Set the success messages
     * 
     * @param array $successMessages Success Messages
     */
    public function setSuccessMessages($successMessages) {
        $this->successMessages = $successMessages;
    }

    /**
     * Append success message
     * 
     * @param string $message Message
     */
    public function appendSuccessMessage($message, $messageContent = null, $duration = null) {
        // upgraded messages - comes with title and structure
        if (is_null($messageContent)) {
            $message = [
                'title' => 'Message',
                'message' => $message,
                'duration' => $messageContent
            ];
        }

        else {
            $message = [
                'title' => $message,
                'message' => $messageContent,
                'duration' => $duration
            ];
        }

        array_push($this->successMessages, $message);
    }

    /**
     * Set the warnings
     * 
     * @param array $warnings Warnings
     */
    public function setWarnings($warnings) {
        $this->warnings = $warnings;
    }

    /**
     * Append warning
     * 
     * @param string $warning Warning
     */
    public function appendWarning($warning, $warningContent = null, $duration = null) {
        // upgraded messages - comes with title and structure
        if (is_null($warningContent)) {
            $warning = [
                'title' => 'Warning',
                'message' => $warning,
                'duration' => $warningContent
            ];
        }

        else {
            $warning = [
                'title' => $warning,
                'message' => $warningContent,
                'duration' => $duration
            ];
        }

        array_push($this->warnings, $warning);
    }

    /**
     * Set the errors
     * 
     * @param array $errors Errors
     */
    public function setErrors($errors) {
        $this->errors = $errors;
    }

    /**
     * Append error
     * 
     * @param string $error Error
     */
    public function appendError($error, $errorContent = null, $duration = null) {
        // upgraded messages - comes with title and structure
        if (is_null($errorContent)) {
            $error = [
                'title' => 'Error',
                'message' => $error,
                'duration' => $errorContent
            ];
        }

        else {
            $error = [
                'title' => $error,
                'message' => $errorContent,
                'duration' => $duration
            ];
        }

        array_push($this->errors, $error);
    }

    /**
     * Set the redirect
     * 
     * @param array
     */
    public function setRedirect($redirect) {
        $this->redirect = $redirect;
    }

    /**
     * Return the response data in JSON.
     */
    public function output() {
        // merge fields
        $this->headers = array_merge($this->headers, [
            'status' => $this->status
        ]);

        asort($this->headers);

        // return the structure as instructed
        return [
            'headers' => $this->headers,
            'response' => [
                'messages' => [
                    'success' => $this->successMessages,
                    'warnings' => $this->warnings,
                    'errors' => $this->errors,
                ],

                'data' => $this->data
            ],

            'redirect' => $this->redirect
        ];
    }
}
