<?php

namespace Whiteboard\Services;

use GuzzleHttp\Client;
use Flow\JSONPath\JSONPath;

class BaseOutput {
	public $rawOutput;
	public $parsedOutput;

    public $isValid = true;

	// initialise the output
	public function __construct($output) {
		try {
            if (gettype($output) == 'string') $structure = json_decode($output);
            else $structure = $output;

            // create JSONPath from given object
            $this->parsedOutput = new JSONPath($output);
        }

        catch (\Exception $e) {
        	$this->isValid = false;
        }
	}

	//  __  __ _____ _____  _    
	// |  \/  | ____|_   _|/ \   
	// | |\/| |  _|   | | / _ \  
	// | |  | | |___  | |/ ___ \ 
	// |_|  |_|_____| |_/_/   \_\
	// 

	// Request Information

    // get objects
    public function getHeaders() { return $this->getPath("headers"); }
    public function getResponse() { return $this->getPath("response"); }
    public function getResponseMessages() { return $this->getPath("response.messages"); }
    public function getResponseBody() { return $this->getPath("response.data"); }
	
    // get single strings
    public function getResponseTime() { return $this->getPath("headers.responseTime"); }
    public function getStatus() { return $this->getPath("headers.status"); }
    public function getService() { return $this->getPath("headers.service"); }
    public function getServerID() { return $this->getPath("headers.serverID"); }

    public function getPath($path) {
        // perform query
        $results = $this->parsedOutput->find($path);

        if (sizeof($results) == 1) {
            return $results[0];
        }

        else if (sizeof($results) > 0) {
            return $results;
        }

        else {
            return null;
        }
    }
}