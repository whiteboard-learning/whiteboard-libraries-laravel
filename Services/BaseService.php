<?php

namespace Whiteboard\Services;

use GuzzleHttp\Client;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\TooManyRedirectsException;

class BaseService {
	// methods
	public static function getServiceInstance(string $name) : string {
		// RETURN A STATIC HOST FOR NOW
		return "http://" . $name . ".whiteboard.pw/";
	}

	public static function _sendRequest(string $serviceName, string $path = "/", $data = []) {
		// validate data
		if (gettype($data) != 'array') return null;

		// service name
		$instance = BaseService::getServiceInstance($serviceName);
		if ($instance == null) return null;

		// create the URL object
		$url = $instance . $path;

		// verify request
		$parsedURL = parse_url($url);
		if (!$parsedURL) return null;

		// create the http client
		$client = new Client([
			'base_uri' => $instance,
		    'headers'  => [ 'Accept' => 'application/json', 'Content-Type' => 'application/x-www-form-urlencoded' ],
		]);

		try {
			$response = $client->post($path, [
				'form_params' => $data
			]);

		} catch (RequestException $e) {
			// networking error
			return null;

		} catch (ConnectException $e) {
			// networking error
			return null;

		} catch (ClientException $e) {
			// 400 errors
			return null;

		} catch (ServerException $e) {
			// 500 errors
			return null;

		} catch (TooManyRedirectsException $e) {
			// too many redirects
			return null;
		}

		// process the response
		return json_decode($response->getBody());
	}

	public static function _sendManualRequest(string $url, $data = []) {
		// validate data
		if (gettype($data) != 'array') return null;

		// verify request
		$parsedURL = parse_url($url);
		if (!$parsedURL) return null;

		// create the http client
		$client = new Client([
			'base_uri' => $parsedURL['scheme'] . '://' . $parsedURL['host'] . ':' . $parsedURL['port'],
		    'headers'  => [ 'Accept' => 'application/json', 'Content-Type' => 'application/x-www-form-urlencoded' ],
		]);

		try {
			$response = $client->post($parsedURL['path'], [
				'multipart' => $data
			]);

		} catch (RequestException $e) {
			// networking error
			return null;

		} catch (ConnectException $e) {
			// networking error
			return null;

		} catch (ClientException $e) {
			// 400 errors
			return null;

		} catch (ServerException $e) {
			// 500 errors
			return null;

		} catch (TooManyRedirectsException $e) {
			// too many redirects
			return null;
		}

		// process the response
		return json_decode($response->getBody());
	}	
}