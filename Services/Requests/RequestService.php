<?php

namespace Whiteboard\Services;

use GuzzleHttp\Client;

use Whiteboard\Services\BaseOutput;

class RequestService extends BaseService {
	// methods
    public static function sendRequest(string $serviceName, string $path = "/", $data = []) : BaseOutput {
    	$result = BaseService::_sendRequest($url, $data);
    	return new BaseOutput($result);
    }

    public static function sendManualRequest(string $url, $data = null) : BaseOutput {
    	$result = BaseService::_sendManualRequest($url, $data);
    	return new BaseOutput($result);
    }
}