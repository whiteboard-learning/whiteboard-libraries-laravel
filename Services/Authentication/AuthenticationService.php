<?php

namespace Whiteboard\Services\Authentication;

use GuzzleHttp\Client;

use Whiteboard\Services\BaseService;
use Whiteboard\Services\BaseOutput;
use Whiteboard\Services\Authentication\Objects\Session;

class AuthenticationService extends BaseService {
	// methods
	public function verifyAccessToken(string $accessToken = NULL) : Session {
		// check if valid
		if ($accessToken == null) return new Session("{}");

		// make request
	    $result = BaseService::_sendRequest("authentication", "/api/session/check", [
	        "accessToken" => $accessToken
	    ]);

	    return new Session($result);
	}
}