<?php

namespace Whiteboard\Services\Authentication\Objects;

use Whiteboard\Services\BaseOutput;

class Session extends BaseOutput {
    // methods to get properties
    // access tokens
    public function getAccessTokenObject() { $this->getPath("response.data.accessToken"); }
    public function getAccessToken() { $this->getPath("response.data.accessToken.token"); }
    public function getAccessTokenExpiry() { $this->getPath("response.data.accessToken.expiry"); }

    // user information
    public function getUser() { $this->getPath("response.data.user"); }
    public function getUserId() { $this->getPath("response.data.user.id"); }
    public function getUserFirstName() { $this->getPath("response.data.user.firstName"); }
    public function getUserLastName() { $this->getPath("response.data.user.lastName"); }
    public function getUserName() { $this->getPath("response.data.user.name"); }
    public function getUserRole() { $this->getPath("response.data.user.role"); }

    // user permissions
    public function getPermissions() { $this->getPath("response.data.permissions"); }
}