Whiteboard Common Libraries: (Manual) Implementation Instructions
===

## Controller (helpers)

**In file `app/Http/Controllers/Controller.php`...**

Add the following line before the `class Controller` line:
```php
use Whiteboard\Services\HTTP\APIResponseService;
use Whiteboard\Services\Authentication\AuthenticationService;
```

Replace the entire class `Controller` contents with:

```php
/**
 * @var App\Services\HTTP\APIResponseService Response Service
 */
public $api;

/**
 * @var App\Auth\AccessToken Access Token Model
 */
public $token;

/**
 * Class constructor for Controller.
 */
public function __construct(Request $request) {
    // save the current request
    $this->request = $request;

    // create response service
    $this->api = new APIResponseService;

    // check for the access token
    $this->token = (new AuthenticationService)->verifyAccessToken($request->input('accessToken'));
}

/**
 * Request termination function for Controller.
 * Used for API requests.
 * 
 * @param string $error Error
 * @return \Illuminate\Http\Response
 */
public function terminate($error = null) {
    // append error if needed
    if (!is_null($error) && is_string($error)) $this->api->appendError($error);

    // return response
    response()->json($this->api->output())->send();
    exit();
}
```

## Exception Handling

**In file `app/Exceptions/Handler.php`...**

Add the following line before the `class Handler` line:
```php
use Whiteboard\Services\HTTP\APIResponseService;
```

Replace the function `render` with:

```php
$api = new APIResponseService();

if ($exception instanceof ValidationException) {
    $api->setStatus("INVALID_INPUT");
    $api->appendError("Invalid input", "There are some fields with invalid input.");

    foreach ($exception->errors() as $key => $error) {
        $api->appendError($key, $error[0]);
    }

    response()->json($api->output())->send();
}

else if (config('app.env') == 'production') {
    $api->setStatus("SERVER_ERROR");
    $api->appendError("Internal Server Error", "Our servers are facing technical difficulties handling your request currently. We're sorry for the inconvenience caused - please try again later.");
    response()->json($api->output())->send();
}

else {
    return parent::render($request, $exception);
}
```