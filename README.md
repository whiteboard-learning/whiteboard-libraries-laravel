Whiteboard Common Libraries - Laravel
===

<!-- MarkdownTOC levels="2,3,4" autoanchor="true" autolink="true" -->

- [Installation](#installation)
- [Components](#components)
- [Usage](#usage)
	- [Services](#services)
		- [Base Service Service \(`Whiteboard\Services\BaseService`\)](#base-service-service-whiteboardservicesbaseservice)
		- [HTTP Request Service \(`Whiteboard\Services\RequestService`\)](#http-request-service-whiteboardservicesrequestservice)
	- [Objects](#objects)
		- [Base Output \(`Whiteboard\Services\BaseOutput`\)](#base-output-whiteboardservicesbaseoutput)
		- [Session \(`Whiteboard\Services\Authentication\Objects\Session`\)](#session-whiteboardservicesauthenticationobjectssession)

<!-- /MarkdownTOC -->

<a id="installation"></a>
## Installation
Installation of this package is two-part.

This repository should ideally be included in this library as a Git **submodule**, and it can be done by:
```
git submodule add -b master git@bitbucket.org:whiteboard-learning/whiteboard-libraries-laravel.git
```

<a id="components"></a>
## Components
The following components are available from this library:

- **Middleware** (`Whiteboard\Middleware`) -- mostly Auth\Authentication-related Middleware
	- **AllowIfAuthenticated** (`Whiteboard\Middleware\AllowIfAuthenticated`)
	- **AllowIfRole** (`Whiteboard\Middleware\Auth\AllowIfRole`)
	- **AllowIfGrantedPermission** (`Whiteboard\Middleware\Auth\AllowIfGrantedPermission`)
	- **AllowIfGuest** (`Whiteboard\Middleware\Auth\AllowIfGuest`)

- **Services** (`Whiteboard\Services`)
	- **HTTP Request Service** (`Whiteboard\Services\RequestService`)
	- **API Response Service** (`Whiteboard\Services\HTTP\APIResponseService`)
	- **Authentication Service** (`Whiteboard\Services\Authentication\AuthenticationService`)

- **Objects** (within `Whiteboard\Services` scope)
	- **BaseOutput** (`Whiteboard\Services\BaseOutput`)
	- **Session** (`Whiteboard\Services\Authentication\Objects\Session`)

<a id="usage"></a>
## Usage
Please read the given [Implementation Instructions](_/IMPLEMENTATION_INSTRUCTIONS.md) for **mandatory** steps to fully implementing Whiteboard's standard API inputs/output schemes.

<a id="services"></a>
### Services

<a id="base-service-service-whiteboardservicesbaseservice"></a>
#### Base Service Service (`Whiteboard\Services\BaseService`)
The following methods are able to return a generic Class:
```php
BaseService::_sendRequest(string $serviceName, string $path = "/", $data = []);
BaseService::_sendManualRequest(string $url, $data = []);
```

<a id="http-request-service-whiteboardservicesrequestservice"></a>
#### HTTP Request Service (`Whiteboard\Services\RequestService`)
The following methods are able to return a `BaseOutput` class:
```php
RequestService::sendRequest(string $serviceName, string $path = "/", $data = []);
RequestService::sendManualRequest(string $url, $data = []);
```

<a id="objects"></a>
### Objects

<a id="base-output-whiteboardservicesbaseoutput"></a>
#### Base Output (`Whiteboard\Services\BaseOutput`)

`BaseOutput` accepts a string or object for input:

```php
$output = new BaseOutput("{'test':'value'}"); // string
$output = new BaseOutput(json_decode(json_encode([ 'test' => 'value' ]))); // object
```

`BaseOutput` methods (not static):
```php
// generic implemented getters
BaseOutput::getHeaders();
BaseOutput::getResponse();
BaseOutput::getResponseMessages();
BaseOutput::getResponseBody();
BaseOutput::getResponseTime();
BaseOutput::getStatus();
BaseOutput::getService();
BaseOutput::getServerID();

// getPath (JSONPath)
BaseOutput::getPath(string $path);
```

Example of `getPath()` in use:
```php
// EXAMPLE JSON PARSED:
//
// {
//     "headers": {
//         "responseTime": "2019-11-09 20:50:44",
//         "status": "VALID",
//         "service": "authentication-service",
//         "serverID": "theroyalstudent.local"
//     },
//     "response": {
//         "messages": {
//             "success": [],
//             "warnings": [],
//             "errors": []
//         },
//         "data": {
//             "accessToken": {
//                 "token": "63058c4fd374bc2fabad91537fa7a65da85b1d1a428c56c39e235b2934c2",
//                 "expiry": "2019-12-03T20:01:40.000000Z"
//             },
//             "user": {
//                 "id": 1,
//                 "firstName": "Bob",
//                 "lastName": "Smith",
//                 "name": "Bob Smith",
//                 "role": "User"
//             },
//             "permissions": []
//         }
//     }
// }

$output->getPath("headers"); // output: { "responseTime": "2019-11-09 20:50:44", "status": "VALID", ... } (type: object)
$output->getPath("headers.status"); // output: "VALID" (type: string)
$output->getPath("response.data.user.id"); // output: 1 (type: integer)
$output->getPath("response.data.user.lastName"); // output: "Smith" (type: string)
```

<a id="session-whiteboardservicesauthenticationobjectssession"></a>
#### Session (`Whiteboard\Services\Authentication\Objects\Session`)

`Session` methods (not static):
```php
Session::getAccessTokenObject(); // -- alias for getPath("response.data.accessToken");
Session::getAccessToken(); // -- alias for getPath("response.data.accessToken.token");
Session::getAccessTokenExpiry(); // -- alias for getPath("response.data.accessToken.expiry");
Session::getUser(); // -- alias for getPath("response.data.user");
Session::getUserId(); // -- alias for getPath("response.data.user.id");
Session::getUserFirstName(); // -- alias for getPath("response.data.user.firstName");
Session::getUserLastName(); // -- alias for getPath("response.data.user.lastName");
Session::getUserName(); // -- alias for getPath("response.data.user.name");
Session::getUserRole(); // -- alias for getPath("response.data.user.role");
Session::getPermissions(); // -- alias for getPath("response.data.permissions");
```